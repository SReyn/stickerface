// ========================================================================= //
// Copyright 2019 SUPAMONKS_STUDIO                                           //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include "stickerface/math.hpp"

namespace stickerface { namespace sphere {
    struct AllParms
    {
        Eigen::Vector3f              rSprtGamma; // 1.0 / gamma
        Eigen::Vector3f              center;
        Eigen::Vector3f              upWPosition;
        int                          numSpriteCols;
        int                          numSpriteRows;
        std::vector<int>             indices;
        std::vector<Eigen::Vector3f> frontWPositions;
        std::vector<Eigen::Vector3f> mappings;
    public:
        inline
        AllParms():
            rSprtGamma     (Eigen::Vector3f::Ones()),
            center         (Eigen::Vector3f::Zero()),
            upWPosition    (Eigen::Vector3f::Zero()),
            numSpriteCols  (0),
            numSpriteRows  (0),
            indices        (),
            frontWPositions(),
            mappings       ()
        { }

        inline
        void
        clear()
        {
            rSprtGamma.setOnes();
            center.setZero();
            upWPosition.setZero();
            numSpriteCols = 0;
            numSpriteRows = 0;
            indices.clear();
            frontWPositions.clear();
            mappings.clear();
        }

        friend inline
        std::ostream&
        operator<<(std::ostream&   out,
                   const AllParms& p)
        {
            out
                << "rcp(gamma) = " << p.rSprtGamma.transpose() << "\n"
                << "center = " << p.center.transpose() << "\n"
                << "up world position = " << p.upWPosition.transpose() << "\n"
                << "# sprites = " << p.numSpriteCols << " x " << p.numSpriteRows << "\n";
            const size_t numSprites = p.indices.size();
            for (size_t i = 0; i < numSprites; ++i)
                out
                    << "sprite " << i << ":"
                    << "\tindex = " << p.indices[i]
                    << "\tfront world pos = " << p.frontWPositions[i].transpose()
                    << "\tmapping = " << p.mappings[i].transpose() << "\n";
            return out;
        }
    };
} }
