// ========================================================================= //
// Copyright 2019 SUPAMONKS_STUDIO                                           //
// Author: Pierre-Edouard Landes <pel@supamonks.com>                         //
//                                                                           //
// This program is free software: you can redistribute it and/or modify      //
// it under the terms of the GNU Lesser General Public License as            //
// published by the Free Software Foundation, either version 3 of the        //
// License, or (at your option) any later version.                           //
//                                                                           //
// This program is distributed in the hope that it will be useful,           //
// but WITHOUT ANY WARRANTY; without even the implied warranty of            //
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             //
// GNU Lesser General Public License for more details.                       //
//                                                                           //
// You should have received a copy of the GNU Lesser General Public License  //
// along with this program.  If not, see <https://www.gnu.org/licenses/>.    //
//                                                                           //
// ========================================================================= //
#pragma once

#include <vector>

namespace VUtils
{
    class AColor;
}

namespace stickerface
{
    class BitmapPNG
    {
    private:
        unsigned                   _width;
        unsigned                   _height;
        std::vector<unsigned char> _image;

    public:
        BitmapPNG();

        operator bool() const;

        void
        clear();

        void
        load(const std::string&);

        void
        get(float,
            float,
            VUtils::AColor&) const;

    private:
        void
        getRawRGBA(int,
                   int,
                   VUtils::AColor&) const;
    };
}
