-- ========================================================================= --
-- Copyright 2019 SUPAMONKS_STUDIO                                           --
-- Author: Pierre-Edouard Landes <pel@supamonks.com>                         --
--                                                                           --
-- This program is free software: you can redistribute it and/or modify      --
-- it under the terms of the GNU Lesser General Public License as            --
-- published by the Free Software Foundation, either version 3 of the        --
-- License, or (at your option) any later version.                           --
--                                                                           --
-- This program is distributed in the hope that it will be useful,           --
-- but WITHOUT ANY WARRANTY; without even the implied warranty of            --
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             --
-- GNU Lesser General Public License for more details.                       --
--                                                                           --
-- You should have received a copy of the GNU Lesser General Public License  --
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.    --
--                                                                           --
-- ========================================================================= --

dofile('path.lua')

--                     _
-- __      _____  _ __| | _____ _ __   __ _  ___ ___
-- \ \ /\ / / _ \| '__| |/ / __| '_ \ / _` |/ __/ _ \
--  \ V  V / (_) | |  |   <\__ \ |_) | (_| | (_|  __/
--   \_/\_/ \___/|_|  |_|\_\___/ .__/ \__,_|\___\___|
--                             |_|

workspace('StickerFace')

    platforms { 'x64' }
    configurations { 'Release' }
    location(_ACTION)

--  _              _      ____  _   _  ____
-- | |    ___   __| | ___|  _ \| \ | |/ ___|
-- | |   / _ \ / _` |/ _ \ |_) |  \| | |  _
-- | |__| (_) | (_| |  __/  __/| |\  | |_| |
-- |_____\___/ \__,_|\___|_|   |_| \_|\____|

project('LodePNG')

    language('C++')
    kind('StaticLib')

    files
    {
        'dep/LodePNG/include/lodepng.h',
        'dep/LodePNG/src/lodepng.cpp',
    }
    includedirs
    {
        'dep/LodePNG/include',
    }

--                  _           _
--  _ __  _ __ ___ (_) ___  ___| |_
-- | '_ \| '__/ _ \| |/ _ \/ __| __|
-- | |_) | | | (_) | |  __/ (__| |_
-- | .__/|_|  \___// |\___|\___|\__|
-- |_|           |__/

project('vray_stickerface')

    language('C++')
    kind('SharedLib')

    files
    {
        'include/**.h',
        'include/**.hpp',
        'src/**.h',
        'src/**.hpp',
        'src/**.cpp',
    }
    defines
    {
        '_HAVE_INTRIN',
        'Bits64_',
        '__VRAY4MAYA30__',
        'VRAY_NOEXPORTS',
        'VRAY_MULTI_MODIFICATION_BUILD',
        'SECURITY_SHADERS',
        'WITH_VISMAT_EDITOR_FOR_NON_ASGVIS_PROJECT',
    }
    if os.get() == 'windows' then
        defines
        {
            '_USE_MATH_DEFINES',
            '_CRT_SECURE_NO_WARNINGS',
            '_CRT_SECURE_NO_DEPRECATE',
            '_CRT_NONSTDC_NO_DEPRECATE',
            'NOMINMAX',
        }
    end

    includedirs
    {
        'include',
        'dep/LodePNG/include',
        eigen_path(),
        vray_sdk_path('include'),
    }
    libdirs
    {
        os.matchdirs(vray_sdk_path('lib', 'x64', '*')),
    }
    links
    {
        'vray',
        'plugman_s',
        'vutils_s',
        'LodePNG',
    }
    -- smks.dep.eigen.includes()
    -- smks.dep.lodepng.links()

    if _ACTION:match('vs%d+') then
        -- visual studio specifics
        buildoptions
        {
            '/MP4',
            '/EHa',
        }
    end

    filter 'configurations:Debug'
        targetsuffix '_d'
        symbols 'On'
        defines
        {
            'DEBUG',
            '_DEBUG',
        }

    filter 'configurations:Release'
        optimize 'On'
        defines
        {
            'NDEBUG',
        }

    filter {}
