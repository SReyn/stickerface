# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #
import logging

LOG = logging.getLogger(__name__)


#  ____             _ _          ___   __  __          _
# / ___| _ __  _ __(_) |_ ___   / _ \ / _|/ _|___  ___| |_ ___
# \___ \| '_ \| '__| | __/ _ \ | | | | |_| |_/ __|/ _ \ __/ __|
#  ___) | |_) | |  | | ||  __/ | |_| |  _|  _\__ \  __/ |_\__ \
# |____/| .__/|_|  |_|\__\___|  \___/|_| |_| |___/\___|\__|___/
#       |_|

def _get(sprt):
    _OFFSETS = {
        'eyes': [0.0, 0.2, 0.0],
        'mouth': [0.0, -0.2, 0.0],
        'eyeball': [-0.25, 0.0, 0.0],
        'brow': [-0.25, 0.4, 0.0],
        'upperEyelid': [0.0, 0.085, 0.0],
        'lowerEyelid': [0.0, -0.085, 0.0],
        'extraTop': [0.0, -0.5, 0.0],
        'extraBack': [-0.18, -0.5, 0.0],
        'extraFront': [0.18, -0.5, 0.0],
    }
    if sprt in _OFFSETS:
        return _OFFSETS[sprt]

    keys = [k for k in _OFFSETS.keys() if k in sprt]
    if keys:
        return _OFFSETS[keys[0]]

    # Try out some custom assignment rules
    _REDIR = {
        'eyelid': 'upperEyelid',
    }
    keys = [k for k in _REDIR.keys() if k in sprt]
    if keys:
        return _OFFSETS[_REDIR[keys[0]]]

    return [0.0] * 3


def get(sprt, scaling=1.0):
    return [scaling * _ for _ in _get(sprt=sprt)]
