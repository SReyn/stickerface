# =========================================================================== #
# Copyright 2019 SUPAMONKS_STUDIO                                             #
# Author: Pierre-Edouard Landes <pel@supamonks.com>                           #
#                                                                             #
# This program is free software: you can redistribute it and/or modify        #
# it under the terms of the GNU Lesser General Public License as              #
# published by the Free Software Foundation, either version 3 of the          #
# License, or (at your option) any later version.                             #
#                                                                             #
# This program is distributed in the hope that it will be useful,             #
# but WITHOUT ANY WARRANTY; without even the implied warranty of              #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               #
# GNU Lesser General Public License for more details.                         #
#                                                                             #
# You should have received a copy of the GNU Lesser General Public License    #
# along with this program.  If not, see <https://www.gnu.org/licenses/>.      #
#                                                                             #
# =========================================================================== #


#  _____                ____        _
# |  ___|_ _  ___ ___  |  _ \  __ _| |_ __ _
# | |_ / _` |/ __/ _ \ | | | |/ _` | __/ _` |
# |  _| (_| | (_|  __/ | |_| | (_| | || (_| |
# |_|  \__,_|\___\___| |____/ \__,_|\__\__,_|

class FaceData(object):
    def __init__(self):
        self.asset_name = ''
        self.namespaces = []
        self.root = ''
        self.ctrl_map = {}  # full path -> controller data
        self.atlas_path = ''
        self.atlas_grid = (0, 0)

    def __repr__(self):
        return '"{} -- {}" {} x {} sprites in {}\n\t{}'.format(
            ':'.join(self.namespaces),
            self.asset_name,
            self.atlas_grid[0],
            self.atlas_grid[1],
            self.atlas_path,
            '\n\t'.join([str(_) for _ in self.ctrl_map.values()]))


#   ____            _             _ _             ____        _
#  / ___|___  _ __ | |_ _ __ ___ | | | ___ _ __  |  _ \  __ _| |_ __ _
# | |   / _ \| '_ \| __| '__/ _ \| | |/ _ \ '__| | | | |/ _` | __/ _` |
# | |__| (_) | | | | |_| | | (_) | | |  __/ |    | |_| | (_| | || (_| |
#  \____\___/|_| |_|\__|_|  \___/|_|_|\___|_|    |____/ \__,_|\__\__,_|

class ControllerData(object):
    def __init__(self):
        self.sprite_name = ''
        self.flags = 0
        self.obj = ''
        self.attr = ''
        self.index_offset = 0
        self.index_range = 0

    def __repr__(self):
        return '"{}": {} . {} from {} to {} (f = {})'.format(
            self.sprite_name,
            self.obj,
            self.attr,
            self.index_offset,
            self.index_range,
            self.flags)
